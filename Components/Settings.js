import React from 'react'
import { StyleSheet, View, Text, ActivityIndicator, ScrollView, Image, TouchableOpacity, Share, Alert, Platform, Button } from 'react-native'
import { connect } from 'react-redux'
import SettingsPage, {
	SectionRow,
  NavigateRow,
  SwitchRow,
  TextInputRow,
	CheckRow
} from './Settings/Pages/SettingsPage'
import { AsyncStorage } from "react-native"
class Settings extends React.Component {

  constructor(props) {
    super(props)
    this._savePrefrences = this._savePrefrences.bind(this)
		this.state = {
			preferences: null
		}
  }



  componentWillUnmount() {
   this._savePrefrences()
  }

	componentDidMount = () => AsyncStorage.getItem('PREFERENCES').then((value) => this.setState({ preferences: JSON.parse(value) }))


  _savePrefrences() {
    const action = { type: "SAVE_PREFERENCES", value: this.state.preferences }
    this.props.dispatch(action)
  }

  render() {
		console.log(this.state.preferences)
		if (this.state.preferences == null) {
			return (
        <View>
          <ActivityIndicator size='large' />
        </View>
      )
		}
		else
		{
			return ( <SettingsPage>
					<SectionRow text='Server'>
					<TextInputRow
						text='Server:'
						_value={this.state.preferences.BACKEND_API}
					 	_style={{iconRight:1 , color:'ghostwhite'}}
						_onChangeText={(text) =>
							this.setState(prevState => {
								let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
								preferences.BACKEND_API = text;                     // update the name property, assign a new value
								return { preferences };                                 // return new object jasper object
							})}
						 />
					</SectionRow >
	        <SectionRow text='Clipboard'>
	        <SwitchRow
	          text='Copy torrent link/magnet in clipboard on clicking sources'
	          iconName='clipboard'
	          _value={this.state.preferences.COPY_CLIPBOARD}
	          style={{backgroundColor:'#141414'}}
	          _onValueChange={() =>
	            this.setState(prevState => {
	              let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
	              preferences.COPY_CLIPBOARD = !prevState.preferences.COPY_CLIPBOARD;                     // update the name property, assign a new value
	              return { preferences };                                 // return new object jasper object
	            })}
	           />
	        </SectionRow>
	      <SectionRow text='Kodi (with Elementum)'>
	      <TextInputRow
	             text='Host:'
	            _style={{iconRight:1 , color:'ghostwhite'}}
	            _onChangeText={(text) => this.setState(prevState => {
	              let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
	              preferences.KODI_HOST = text;                     // update the name property, assign a new value
	              return { preferences };                                 // return new object jasper object
	            })}
	            _value={this.state.preferences.KODI_HOST}
	            />
	        <SwitchRow
	          text='Stream torrent on clicking sources'
	          iconName='play'
	          _value={this.state.preferences.KODI_ENABLED}
	          style={{backgroundColor:'#141414'}}
	          _onValueChange={() =>
	            this.setState(prevState => {
	              let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
	              preferences.KODI_ENABLED = !prevState.preferences.KODI_ENABLED;                     // update the name property, assign a new value
	              return { preferences };                                 // return new object jasper object
	            })}
	           />
	      </SectionRow>
	      <SectionRow text='SeedBox.fr'>
	      <TextInputRow
	             text='Host:'
	            _style={{iconRight:1 , color:'ghostwhite'}}
	            _onChangeText={(text) => this.setState(prevState => {
	              let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
	              preferences.SEEDBOXFR_HOST = text;                     // update the name property, assign a new value
	              return { preferences };                                 // return new object jasper object
	            })}
	            _value={this.state.preferences.SEEDBOXFR_HOST}
	            />
	      <TextInputRow
	             text='Username:'
	            _style={{iconRight:1 , color:'ghostwhite'}}
	            _onChangeText={(text) => this.setState(prevState => {
	              let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
	              preferences.SEEDBOXFR_USERNAME = text;                     // update the name property, assign a new value
	              return { preferences };                                 // return new object jasper object
	            })}
	            _value={this.state.preferences.SEEDBOXFR_USERNAME}
	            />
	            <TextInputRow
	             text='Password:'
	            _style={{iconRight:1 , color:'ghostwhite'}}
	            _onChangeText={(text) => this.setState(prevState => {
	              let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
	              preferences.SEEDBOXFR_PASSWORD = text;                     // update the name property, assign a new value
	              return { preferences };                                 // return new object jasper object
	            })}
	            _value={this.state.preferences.SEEDBOXFR_PASSWORD}
	            />
	             <TextInputRow
	             text='Download folder:'
	            _style={{iconRight:1 , color:'ghostwhite'}}
	            _onChangeText={(text) => this.setState(prevState => {
	              let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
	              preferences.SEEDBOXFR_DOWNLOAD_DIR = text;                     // update the name property, assign a new value
	              return { preferences };                                 // return new object jasper object
	            })}
	            _value={this.state.preferences.SEEDBOXFR_DOWNLOAD_DIR}
	            />
	        <SwitchRow
	          text='Add torrent to download on the remote seedbox on clicking sources'
	          iconName='rocket'
	          _value={this.state.preferences.SEEDBOXFR_ENABLED}
	          style={{backgroundColor:'#141414'}}
	          _onValueChange={() => {
	            this.setState(prevState => {
								if (this.state.preferences.SEEDBOXFR_DOWNLOAD_DIR ==  "" &&
								this.state.preferences.SEEDBOXFR_PASSWORD == "" &&
								this.state.preferences.SEEDBOXFR_USERNAME == "" &&
								this.state.preferences.SEEDBOXFR_HOST == ""
								)
								return;
	              let preferences = Object.assign({}, prevState.preferences);  // creating copy of state variable jasper
	              preferences.SEEDBOXFR_ENABLED = !prevState.preferences.SEEDBOXFR_ENABLED;                     // update the name property, assign a new value
	              return { preferences };                                 // return new object jasper object
	            })}}
	           />

	      </SectionRow>
	    </SettingsPage>)
		}

  }
}

const styles = StyleSheet.create({

})



export default connect()(Settings)
