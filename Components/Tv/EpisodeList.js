// Components/FilmList.js

import React from 'react'
import { StyleSheet, FlatList, View,Dimensions } from 'react-native'
import EpisodeItem from './EpisodeItem'
import { connect } from 'react-redux'
import {TV_CATEGORIE} from '../../API/TorrentFinder'
class EpisodeList extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      episodes: [],
      orientation: '',
      refresh: false
    }
  }




  getOrientation(){
    if( Dimensions.get('window').width < Dimensions.get('window').height )
      {
        this.setState({ orientation: 'portrait' });
      }
      else
      {
        this.setState({ orientation: 'landscape' });
      }
  }

  componentDidMount(){
    this.getOrientation();

    Dimensions.addEventListener( 'change', () =>
    {
      this.getOrientation();
      this.setState({ refresh: !this.state.refresh });
    });

  }

  _searchSourcesForEpisode = (episode) => {

      console.log("search sources ", this.props.tv.name, episode.season_number,episode.episode_number )
      this.props.navigation.navigate('TorrentResult',
      {
        keyword: this.props.tv.name,
        keyword_callback: this.props.tv.original_name,
        categorie: TV_CATEGORIE,
        season_number: episode.season_number,
        episode_number: episode.episode_number
      })
  }

  render() {
      return (
        <FlatList
          style={styles.list}
          data={this.props.episodes}
          keyExtractor={(item) => item.id.toString()}
          extraData={this.state.refresh}
          renderItem={({item}) => (
            <EpisodeItem
              episode={item}
              orientation = {this.state.orientation}
              onPress={this._searchSourcesForEpisode}
            />
          )}
        />)
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: "#141414"
  }
})



export default connect()(EpisodeList)
