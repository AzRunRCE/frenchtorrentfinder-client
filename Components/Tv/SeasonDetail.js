import React from 'react'
import { StyleSheet, View, Text, ActivityIndicator, ScrollView, Image, TouchableOpacity, Share, Alert, Platform, Button } from 'react-native'
import { getTvSeasonFromApi, getImageFromApi } from '../../API/TMDBApi'
import moment from 'moment'
import numeral from 'numeral'
import { connect } from 'react-redux'
import EnlargeShrink from '../../Animations/EnlargeShrink'
import EpisodeList from './EpisodeList'
class SeasonDetail extends React.Component {

  static navigationOptions = ({ navigation }) => {
      const { params } = navigation.state
      if (params.media != undefined && Platform.OS === 'ios') {
        return {
            headerRight: <TouchableOpacity
                            style={styles.share_touchable_headerrightbutton}
                            onPress={() => params.sharemedia()}>
                            <Image
                              style={styles.share_image}
                              source={require('../../Images/ic_share.png')} />
                          </TouchableOpacity>
        }
      }
  }

  constructor(props) {
    super(props)
    this.state = {
      episodes: undefined,
      isLoading: false
    }

  }

  _updateNavigationParams() {
    this.props.navigation.setParams({
      sharemedia: this._sharemedia,
      episodes: this.state.episodes
    })
  }

  componentDidMount() {
    this.setState({ isLoading: true })
    getTvSeasonFromApi(this.props.navigation.state.params.media.id, this.props.navigation.state.params.season_number).then(data => {
      this.setState({
        episodes: data.episodes,
        isLoading: false
      }, () => { this._updateNavigationParams() })
    })
  }

  _displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }





  _displaymedia() {
      return (
        <ScrollView style={styles.scrollview_container}>
          <EpisodeList
            episodes={this.state.episodes}
            tv={this.props.navigation.state.params.media}
            navigation={this.props.navigation}>
          </EpisodeList>
        </ScrollView>
      )
  }


  render() {
    return (
      <View style={styles.main_container}>
        {this._displayLoading()}
        {this._displaymedia()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#141414'
  },
  scrollview_container: {
    flex: 1,
    backgroundColor: '#141414'
  },
  image: {
    height: 169,
    margin: 5
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 35,
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 10,
    color: 'white',
    textAlign: 'center'
  },
  favorite_container: {
    alignItems: 'center',
  },
  description_text: {
    fontStyle: 'italic',
    color: 'white',
    margin: 5,
    marginBottom: 15
  },
  default_text: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
    color: 'white',
  },
  favorite_image:{
    flex: 1,
    width: null,
    height: null
  },
  share_touchable_floatingactionbutton: {
    position: 'absolute',
    width: 60,
    height: 60,
    right: 30,
    bottom: 30,
    borderRadius: 30,
    backgroundColor: '#e91e63',
    justifyContent: 'center',
    alignItems: 'center'
  },
  share_touchable_headerrightbutton: {
    marginRight: 8
  },
  share_image: {
    width: 30,
    height: 30
  }
})

const mapStateToProps = (state) => {
  return {
    favoritesMedia: state.toggleFavorite.favoritesMedia
  }
}

export default connect(mapStateToProps)(SeasonDetail)
