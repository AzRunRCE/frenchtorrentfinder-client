import { StyleSheet } from 'react-native'

const SettingsStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000'
  },
  content: {
    flex: 1,
  }
})

export default SettingsStyle