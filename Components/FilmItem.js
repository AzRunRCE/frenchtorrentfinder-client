// Components/FilmItem.js

import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, Dimensions } from 'react-native'
import { getImageFromApi } from '../API/TMDBApi'
import FadeIn from '../Animations/FadeIn'

class FilmItem extends React.Component {
  
  render() {
    const { media, onPress, orientation,section } = this.props
    const {width, height}  = Dimensions.get('window')  
    return (
      <FadeIn>
         <View style={styles.MainContainer}>
          <TouchableOpacity
          onPress={() => onPress(section,media.id)}>
          <Image
            style={{width:  ((width - 30) / 3) ,
              height:  ((width - 30) / 3) * 180 / 120,
              margin: 5}}
            source={{uri: getImageFromApi(media.poster_path)}}
          />
          </TouchableOpacity>
        </View>
      </FadeIn>
    )
  }

 

}

const styles = StyleSheet.create({
  
  main_container: {
    backgroundColor: '#141414',
  
  },
  content_container: {
    flex: 1,
    margin: 5,
    backgroundColor: '#141414'
  },
  header_container: {
    flex: 3,
    flexDirection: 'row',
    backgroundColor: '#141414'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    color:'white',
    flexWrap: 'wrap',
    paddingRight: 5,
    backgroundColor: '#141414'
  },
  vote_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: 'white',
    backgroundColor: '#141414'
  },
  description_container: {
    flex: 7
  },
  description_text: {
    fontStyle: 'italic',
    color: 'white'
  },
  date_container: {
    flex: 1
  },
  date_text: {
    textAlign: 'right',
    fontSize: 14,
    color:'white',
  },
  favorite_image: {
    width: 25,
    height: 25,
    marginRight: 5
  }
})

export default FilmItem