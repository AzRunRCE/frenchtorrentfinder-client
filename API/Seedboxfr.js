// API/KODI.js
import RNFetchBlob from 'rn-fetch-blob'


export function ParseX_Transmission_Session_Id (src) {
  try {
  const regex = /<code>.*<\/code>/gm;
  found = src.toString().match(regex);
  if (found !== null){
    token = found[0]
    token = token.replace('<code>X-Transmission-Session-Id: ','')
    token = token.replace('</code>','')
    return token;
  }
  else {
    throw new Error('Cant parse the csrf token. Please check your credentials settings for SeedBox.fr');
  }

}
catch (e) {
  console.error(e.message);
}
}

export function GetX_Transmission_Session_Id (HOST,credentials) {
  var data = '{ "method" : "torrent-get", "arguments" : { "fields" : ["id"] } }'
  return RNFetchBlob.config({
    trusty : true
  }).fetch('POST', HOST, {
      Authorization :credentials,
      'Content-Type': 'application/json',
      'X-Transmission-Session-Id': 'FAKE_TOKEN',
      },data)
  .then((res) => {
    return ParseX_Transmission_Session_Id(res.text())
  })
  .catch((error) =>  console.error(error));
}

export async function  downloadTorrent (HOST,credentials,path, torrentUri) {
  try {
    var token = await GetX_Transmission_Session_Id(HOST, credentials)
    var data = JSON.stringify({
      "method": "torrent-add",
      "arguments": {
        "filename": torrentUri,
        "download-dir": path,
        "paused": false
      },
      "tag": ""
    });
    return RNFetchBlob.config({
      trusty : true
    }).fetch('POST', HOST, {
        Authorization :credentials,
        'Content-Type': 'application/json',
        'X-Transmission-Session-Id': token,
      }, data)
     .then((response) => response.json())
     .then((responseJson) => {
       if (responseJson.result === "success"){
        return responseJson.arguments;
       }
    })
    .catch((error) => {  console.error(error); return false;});
  }
  catch (e) {
    console.error('error');
  }
}
