

[![FrenchTorrentFinder Logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/13881525/web_hi_res_512.png?width=64)](https://gitlab.com/AzRunRCE/frenchtorrentfinder-client)

Ios [![Ios Build status](https://build.appcenter.ms/v0.1/apps/249a30c8-3104-4d41-8691-7182af770ea8/branches/master/badge)](https://appcenter.ms)

Android [![Android Build status](https://build.appcenter.ms/v0.1/apps/b1586a4e-fa9a-4ffb-b562-445e86b8f608/branches/master/badge)](https://appcenter.ms)

Android (GitLab) [![pipeline status](https://gitlab.com/AzRunRCE/frenchtorrentfinder-client/badges/master/pipeline.svg)](https://gitlab.com/AzRunRCE/frenchtorrentfinder-client/commits/master)

![alt text](https://gitlab.com/AzRunRCE/frenchtorrentfinder/raw/master/Screenshots/demo_main.gif "Main view")
![alt text](https://gitlab.com/AzRunRCE/frenchtorrentfinder/raw/master/Screenshots/settings.PNG "Settings view")

## [APK Download link (Artifacts)](https://gitlab.com/AzRunRCE/frenchtorrentfinder-client/pipelines/latest)

## Installation and Run

* Clone project
* `npm i`
* Set your API TMDB Token in /API/TMDBApi.js file
* `react-native run-ios` or `react-native run-android`

Check [TMDB API](https://www.themoviedb.org/documentation/api) for more informations about querying films.