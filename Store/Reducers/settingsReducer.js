// Store/Reducers/favoriteReducer.js
import { AsyncStorage } from "react-native"
const PREFERENCES_KEY = "PREFERENCES"
const initialState = {
    BACKEND_API: "https://azrunsoft.ddns.net/",
    KODI_ENABLED: false,
    KODI_HOST: "http://192.168.0.18:65220",
    COPY_CLIPBOARD: false,
    SEEDBOXFR_ENABLED : true,
    SEEDBOXFR_HOST: "https://seed95.my-seedbox.com:20550/transmission/rpc",
    SEEDBOXFR_USERNAME: "totofkofun",
    SEEDBOXFR_PASSWORD: "",
    SEEDBOXFR_DOWNLOAD_DIR: "/data/client_10550/outp/Plex",
}

function savePreferences(state = initialState, action) {
  let nextState
  console.log(action)
  switch (action.type) {
    case 'SAVE_PREFERENCES':
     AsyncStorage.setItem(PREFERENCES_KEY, JSON.stringify(action.value));
      nextState = {
        ...state,
        preferences: action.value,
      }
      return nextState || state
  default:
    return state
  }
}

AsyncStorage.getItem(PREFERENCES_KEY)
.then((value) =>{
 if (value == null) {
    console.log("save first")
    AsyncStorage.setItem(PREFERENCES_KEY, JSON.stringify(initialState));
 }
})

export default savePreferences
