// API/TorrentFinder.js

import {PREFERENCES_KEY} from '../Store/Reducers/settingsReducer'
export const MOVIES_CATEGORIE = 'Movies/'
export const TV_CATEGORIE = 'Series/'
import { AsyncStorage } from "react-native"
export function findTorrentsByTitle (text,keyword_callback, categorie = "", useCache = true) {
  return AsyncStorage.getItem("PREFERENCES")
  .then((value) =>{
    preferences = JSON.parse(value)
    //console.log(preferences)
    url = preferences.BACKEND_API   + '/api/Torrent/Search/'+ categorie + text + '?json=true&useCache=' + useCache
    console.log(url)
    return fetch(url)
    .then((response) => response.json())
    .then((result) => {
        if(result.length == 0){
          url = preferences.BACKEND_API  + '/api/Torrent/Search/'+ categorie + keyword_callback + '?json=true&useCache=' + useCache
          return fetch(url)
          .then((response) => response.json())
          .catch((error) => console.error(error));
        }
        else
          return result;
      }
    )
    .catch((error) => console.error(error));
  })

}

export function findTvSerieTorrentsByTitle (text, keyword_callback, season_number, episode_number,useCache = true) {
  return AsyncStorage.getItem("PREFERENCES")
  .then((value) =>{
      preferences = JSON.parse(value)
      console.log(preferences)
      url = preferences.BACKEND_API + '/api/torrent/search/series/' + text + '/season/' + season_number + '/episode/' + episode_number +'?json=true&useCache=' + useCache
      return fetch(url)
      .then((response) => response.json())
      .then((result) => {
          if(result.length == 0){
            url = preferences.BACKEND_API  + '/api/torrent/search/series/' + keyword_callback + '/season/' + season_number + '/episode/' + episode_number +'?json=true&useCache=' + useCache
            return fetch(url)
            .then((response) => response.json())
            .catch((error) => console.error(error));
          }
          else
            return result;
        }
      )
      .catch((error) => console.error(error));
  })
}


export function getTorrentSource (id) {
  return AsyncStorage.getItem("PREFERENCES")
  .then((value) =>{
    preferences = JSON.parse(value)
    console.log(preferences)
    const url = preferences.BACKEND_API + '/api/Torrent/' + id + '?json=true'
    console.log(url)
    return fetch(url)
    .then(response => {
      console.log(JSON.stringify(response, null, 4))
      return response.json()})
    .catch((error) => console.error(error));
  })
}
