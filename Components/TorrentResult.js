// Components/FilmDetail.js

import React from 'react'
import { StyleSheet, View, Text, ActivityIndicator, ScrollView, Image, TouchableOpacity, Share, Alert, Platform, Button } from 'react-native'
import { getFilmDetailFromApi, getImageFromApi } from '../API/TMDBApi'
import { findTorrentsByTitle,findTvSerieTorrentsByTitle, MOVIES_CATEGORIE, SERIES_CATEGORIE } from '../API/TorrentFinder'
import moment from 'moment'
import numeral from 'numeral'
import { connect } from 'react-redux'
import EnlargeShrink from '../Animations/EnlargeShrink'
import TorrentList from './TorrentList';
import Toast from 'react-native-simple-toast';
import {TV_CATEGORIE} from '../API/TorrentFinder'
class TorrentResult extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      torrents: [],
      isLoading: false
    }
    this._loadTorrents = this._loadTorrents.bind(this)
    this._onRefresh = this._onRefresh.bind(this)

  }
  _displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }

  _loadTorrents(useCache) {
    const { navigation } = this.props;
    this.setState({ isLoading: true })
    const keyword = navigation.getParam('keyword', null);
    const keyword_callback = navigation.getParam('keyword_callback', null);

    const categorie = navigation.getParam('categorie', MOVIES_CATEGORIE);

    if (categorie === TV_CATEGORIE){
      const episode_number = navigation.getParam('episode_number', null);
      const season_number = navigation.getParam('season_number', null);
      findTvSerieTorrentsByTitle(keyword, keyword_callback, season_number,episode_number , useCache).then(data => {
         if (data && data.length > 0){
            this.setState({
              torrents: [ ...data ],
              isLoading: false
            })
            console.log(data);
          }
          else {
            Toast.show('No source result for this TV series.', Toast.LONG);
            this.props.navigation.goBack(null);
          }

        }).catch((error) =>{
          console.log(error);
        })
    }
    else if (categorie === MOVIES_CATEGORIE){
        findTorrentsByTitle(keyword,keyword_callback, categorie, useCache).then(data => {
            if (data && data.length > 0){
              this.setState({
                torrents: [ ...data ],
                isLoading: false
              })
              console.log(data);
            }
            else {
              Toast.show('No source result for this TV series.', Toast.LONG);
              this.props.navigation.goBack(null);
            }

          }).catch((error) =>{
            console.log(error);
          })
      }
  }

  _onRefresh() {
    this._loadTorrents(false)
  }

  componentDidMount() {
    this._loadTorrents(true)
  }

  render() {
    return (
      <View style={styles.main_container}>
         <TorrentList
          torrents={this.state.torrents}
          navigation={this.props.navigation}
          onRefresh= {this._onRefresh}
          isLoading ={this.state.isLoading}
         />
        {this._displayLoading()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    backgroundColor: '#141414'
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#141414'
  },
  scrollview_container: {
    flex: 1,
    backgroundColor: '#141414'
  },
  image: {
    height: 169,
    margin: 5
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 35,
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 10,
    color: 'white',
    textAlign: 'center'
  },
  favorite_container: {
    alignItems: 'center',
  },
  description_text: {
    fontStyle: 'italic',
    color: 'white',
    margin: 5,
    marginBottom: 15
  },
  default_text: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
    color: 'white',
  },
  favorite_image:{
    flex: 1,
    width: null,
    height: null
  },
  share_touchable_floatingactionbutton: {
    position: 'absolute',
    width: 60,
    height: 60,
    right: 30,
    bottom: 30,
    borderRadius: 30,
    backgroundColor: '#e91e63',
    justifyContent: 'center',
    alignItems: 'center'
  },
  share_touchable_headerrightbutton: {
    marginRight: 8
  },
  share_image: {
    width: 30,
    height: 30
  }
})

const mapStateToProps = (state) => {
  return {
    favoritesFilm: state.toggleFavorite.favoritesFilm
  }
}

export default connect(mapStateToProps)(TorrentResult)
