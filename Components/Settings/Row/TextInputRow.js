// Dependencies import
import React, { Component } from 'react'
import { View, Text, TouchableOpacity , TextInput, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import SettingsRowStyle from '../Styles/SettingsRowStyle'

// Styles deconstructing
const {
    containerInSection,
    containerInnerSection,
    checkSt,
    iconLeft,
    textLeft,
    text,
} = SettingsRowStyle
const BLUE = "#428AF8";
const LIGHT_GRAY = "#D3D3D3";
// Class for textInput rows
class TextInputRow extends Component {

    state = {
        isFocused: false
      };
    
      handleFocus = event => {
        this.setState({ isFocused: true });
        if (this.props.onFocus) {
          this.props.onFocus(event);
        }
      };
    
      handleBlur = event => {
        this.setState({ isFocused: false });
        if (this.props.onBlur) {
          this.props.onBlur(event);
        }
      };
    

    render() {
        const { isFocused } = this.state;
    const { onFocus, onBlur, ...otherProps } = this.props;
        return (
            <TouchableOpacity onPress={this.props.onPressCallback}>
                <View style={containerInSection}>
                    <View style={containerInnerSection}>
                    <Icon name={this.props.iconName} size={24} style={iconLeft} />
                        <Text style={text} numberOfLines={1} ellipsizeMode={'tail'}>
                            {this.props.text}
                        </Text>
                        <TextInput
                        selectionColor={BLUE}
                        underlineColorAndroid={
                            isFocused ? BLUE : LIGHT_GRAY
                        }
                        onChangeText={(text) => this.props._onChangeText(text)}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        style={styles.textInput}
                        {...otherProps}
                        value={this.props._value}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    textInput: {
      height: 40,
        flex:8,
        flexDirection: 'row',
      paddingLeft: 6,
      color:'ghostwhite'
    }
  });
  
// Component export
export { TextInputRow }