// Components/FilmItem.js

import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, Dimensions } from 'react-native'
import { getImageFromApi } from '../../API/TMDBApi'
import FadeIn from '../../Animations/FadeIn'
import Moment from 'moment/min/moment-with-locales'



class SeasonItem extends React.Component {

  getPicture(season) {
    console.log(season)
    if (season.poster_path === null || season.poster_path == ""){
      return (  <Image
        style={styles.image}
        source={require('../../Images/blank_image.png')}
      />)
    } else {
        return (  <Image
          style={styles.image}
          source={{uri: getImageFromApi(season.poster_path)}}
        />)
    }
  }


  render() {
    const { season,media, onPress } = this.props
    Moment.locale('fr');
    return (
      <FadeIn>
        <TouchableOpacity
          style={styles.main_container}
          onPress={() => onPress(season.season_number)}>
          {this.getPicture(season)}
          <View style={styles.content_container}>
            <View style={styles.header_container}>
              <Text style={styles.title_text}>{season.name + ' | ' + season.episode_count + ' Épisodes'}</Text>
              <Text style={styles.vote_text}>{season.vote_average}</Text>
            </View>
            <View style={styles.description_container}>
              <Text style={styles.description_text} numberOfLines={6}>{season.overview}</Text>
            </View>
            <View style={styles.date_container}>
              <Text style={styles.date_text} numberOfLines={2}>{season.air_date == null ? "Pas de date sortie" : "Saison " + season.season_number + " diffusée le " + Moment(season.air_date).format("ll")}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </FadeIn>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 190,
    flexDirection: 'row',
    backgroundColor: '#141414'
  },
  image: {
    width: 120,
    height: 180,
    margin: 5
  },
  content_container: {
    flex: 1,
    margin: 5,
    backgroundColor: '#141414'
  },
  header_container: {
    flex: 3,
    flexDirection: 'row',
    backgroundColor: '#141414'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    color:'white',
    flexWrap: 'wrap',
    paddingRight: 5,
    backgroundColor: '#141414'
  },
  vote_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: 'white',
    backgroundColor: '#141414'
  },
  description_container: {
    flex: 7
  },
  description_text: {
    fontStyle: 'italic',
    color: 'ghostwhite'
  },
  date_container: {
    flex: 1
  },
  date_text: {
    textAlign: 'left',
    fontSize: 14,
    color:'white',
  },
  favorite_image: {
    width: 25,
    height: 25,
    marginRight: 5
  }
})


export default SeasonItem
