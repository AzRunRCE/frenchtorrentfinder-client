// Components/FilmItem.js

import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import FadeIn from '../Animations/FadeIn'

class TorrentItem extends React.Component {

  _getIconProvider(provider){
    switch (provider) {
      case 'Cpasbien':
          return require('../Images/Cpasbien.png')
          break;
      case 'Torrent9':
          return require('../Images/Torrent9.png')
          break;
      case 'Yggtorrent':
          return require('../Images/Yggtorrent.png')
          break;
      default:

    }
  }

  render() {
    const { torrent, _transfertTorrentToKodi, index } = this.props
  
    return (
      <FadeIn>
        <TouchableOpacity
          style={styles.main_container}
          onPress={() => _transfertTorrentToKodi(index)}
         >
          <View style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'stretch',
        margin: 2
      }}>
            <View  style={styles.image_container}>
              <Image style={styles.image} source={this._getIconProvider(torrent.provider)} />

            </View>
            <Text style={styles.title_text}>{torrent.title}</Text>
            <View style={styles.description_container}>
              <Text style={styles.size_text}>{torrent.size}</Text>
              <Text style={styles.leech_text} >{'Leech: ' + torrent.peers}</Text>
              <Text style={styles.seed_text} >{'Seed: ' + torrent.seeds}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </FadeIn>
    )
  }
}

const styles = StyleSheet.create({
  image_container: {
    alignContent:"space-around",
    width: '15%',
    alignItems: 'center',
    justifyContent:'center',
  },
  image: {
    width: 60,
    height: 25,
    margin: 5,
    resizeMode: 'stretch',
    alignItems: 'center',
    justifyContent:'center',
  },
  title_text: {
    width: '70%',
    fontWeight: 'bold',
    fontSize: 14,
    padding: 2,
    color:'white',
    flexWrap: 'wrap',
    backgroundColor: '#141414',
    alignContent: "center",
    textAlignVertical: "center"
  },
  size_text: {
    fontWeight: 'bold',
    fontSize: 10,
    color: 'dodgerblue',
    backgroundColor: '#141414',
  },
  leech_text: {
    fontWeight: 'bold',
    fontSize: 10,
    color: 'darkred',
    backgroundColor: '#141414',
  },
  seed_text: {
    fontWeight: 'bold',
    fontSize: 10,
    color: 'darkseagreen',
    backgroundColor: '#141414',
  },
  description_container: {
    width: '20%',
    flex:1,
    flexDirection: "column",
    textAlign: "right"
  },
  description_text: {
    fontStyle: 'italic',
    color: 'white'
  },
  date_container: {
    flex: 1
  },
  date_text: {
    textAlign: 'right',
    fontSize: 14,
    color:'white',
  },
  favorite_image: {
    width: 25,
    height: 25,
    marginRight: 5
  }
})

export default TorrentItem
