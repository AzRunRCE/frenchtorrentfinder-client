// Components/Favorites.js

import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import FilmList from './FilmList'
import { connect } from 'react-redux'
import Avatar from './Avatar'
import BaseScreen from './BaseScreen'
class Favorites extends BaseScreen {

  render() {
    return (
      <View style={styles.main_container}>
        <FilmList
          medias={this.props.favoritesMedia}
          navigation={this.props.navigation}
          favoriteList={true}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1
  },
  avatar_container: {
    alignItems: 'center'
  }
})

const mapStateToProps = (state) => {
  return {
    favoritesMedia: state.toggleFavorite.favoritesMedia
  }
}

export default connect(mapStateToProps)(Favorites)
