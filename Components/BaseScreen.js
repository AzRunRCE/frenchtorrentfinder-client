// Components/Favorites.js

import React from 'react'
import { StyleSheet, View, TextInput, Button, Text, FlatList, ActivityIndicator, Image } from 'react-native'
import SettingsButton  from './SettingsButton'
export default class BaseScreen extends React.Component {
  
  

  static navigationOptions = ({ navigation }) => {
   
    return {

      headerRight: (
        <SettingsButton
        _displayFavoriteImage= {() =>  navigation.navigate('Settings')}
        />
      ),
    };
  };

  render(){
    return <View></View>
  }
}


