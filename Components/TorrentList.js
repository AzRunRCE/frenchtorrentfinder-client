// Components/FilmList.js

import React from 'react'
import { View, StyleSheet,Clipboard, FlatList } from 'react-native'
import TorrentItem from './TorrentItem'
import { connect } from 'react-redux'
import { getTorrentSource } from '../API/TorrentFinder'
import { streamTorrent } from '../API/KODI'
import Toast from 'react-native-simple-toast';
import { downloadTorrent,GetX_Transmission_Session_Id } from '../API/Seedboxfr'
import base64 from 'react-native-base64'
import { AsyncStorage } from "react-native"
class TorrentList extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      torrents: []
    }

  }

  _transfertTorrentToKodi = (idTorrent) => {
    AsyncStorage.getItem("PREFERENCES")
    .then((value) =>{
      preferences = JSON.parse(value)
    var source = getTorrentSource(idTorrent).then(data => {
      if (preferences.KODI_ENABLED){
        console.log("KODI_ENABLED")
        streamTorrent(preferences.KODI_HOST, data.value).then(result => {
          if (result == true){
            Toast.show('The streaming should start soon...', Toast.LONG);
          }
        });
      }
      if (preferences.COPY_CLIPBOARD){
        console.log("clipboard")
        Clipboard.setString(data.value);
        Toast.show('The torrent have been copied into the clipboard', Toast.SHORT);
      }

      if (preferences.SEEDBOXFR_ENABLED){
        var credentials ='Basic ' + base64.encode(preferences.SEEDBOXFR_USERNAME + ':' + preferences.SEEDBOXFR_PASSWORD);
      
        var suffixPath = ''
        if(data.categorie === 'Tv'){
           suffixPath = "/Series"
        }
        else if(data.categorie === 'Movies'){
          suffixPath = "/Films"
        }
        downloadTorrent(
          preferences.SEEDBOXFR_HOST,
          credentials,
          preferences.SEEDBOXFR_DOWNLOAD_DIR + suffixPath,
          data.value).then(result => {
            if (result['torrent-duplicate'] !== undefined){
              Toast.show("Torrent Duplicate:" + result['torrent-duplicate'].name, Toast.LONG);
            }
            else if (result['torrent-added'] !== undefined){
              Toast.show("The downloading has started:" + result['torrent-added'].name, Toast.LONG);
            }
          });
      }
    })
  }
    )
  }

  render() {
    return (
        <FlatList
          refreshing= {this.props.isLoading}
          onRefresh={() => this.props.onRefresh()}
          style={styles.list}
          data={this.props.torrents}
          renderItem={({item, index}) => (
            <TorrentItem
              torrent={item}
              _transfertTorrentToKodi = {this._transfertTorrentToKodi}
              index={index}
            />
          )}
        />
    )
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: "#141414"
  }
})


export default connect()(TorrentList)
