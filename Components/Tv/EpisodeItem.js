// Components/FilmItem.js

import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, Dimensions } from 'react-native'
import { getImageFromApi } from '../../API/TMDBApi'
import FadeIn from '../../Animations/FadeIn'
import Moment from 'moment/min/moment-with-locales'



class EpisodeItem extends React.Component {

  getPicture(season) {
    console.log(season)
    if (season.still_path === null || season.still_path == ""){
      return (  <Image
        style={styles.image}
        source={require('../../Images/blank_image.png')}
      />)
    } else {
        return (  <Image
          style={styles.image}
          source={{uri: getImageFromApi(season.still_path)}}
        />)
    }
  }


  render() {
    const { episode, onPress } = this.props
    Moment.locale('fr');
    return (
      <FadeIn>
        <TouchableOpacity
          style={styles.main_container}
          onPress={() => onPress(episode)}>
          {this.getPicture(episode)}
          <View style={styles.content_container}>
            <View style={styles.header_container}>
              <Text style={styles.title_text} numberOfLines={2}>{episode.episode_number + ' - ' + episode.name }</Text>
            </View>
            <View style={styles.description_container}>
              <Text style={styles.description_text} numberOfLines={5}>{episode.overview}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </FadeIn>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 130,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    backgroundColor: '#141414'
  },
  image: {
    width: 180,
    height: 120,
    margin: 5
  },
  content_container: {
    flex: 1,
    margin: 5,
    backgroundColor: '#141414'
  },
  header_container: {
    flex: 3,
    flexDirection: 'row',
    backgroundColor: '#141414'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 14,
    flex: 1,
    color:'white',
    flexWrap: 'wrap',
    paddingRight: 5,
    backgroundColor: '#141414',
    alignContent: "center",
    textAlignVertical: "center"
  },
  vote_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: 'white',
    backgroundColor: '#141414'
  },
  description_container: {
    flex: 7,
    flexWrap: 'nowrap'
  },
  description_text: {
    fontStyle: 'italic',
    color: 'ghostwhite'
  },
  date_container: {
    flex: 1
  },
  date_text: {
    textAlign: 'left',
    fontSize: 14,
    color:'white',
  },
  favorite_image: {
    width: 25,
    height: 25,
    marginRight: 5
  }
})


export default EpisodeItem
