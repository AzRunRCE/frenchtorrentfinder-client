// Store/Reducers/favoriteReducer.js

const initialState = { favoritesMedia: [] }

function toggleFavorite(state = initialState, action) {
  let nextState
  switch (action.type) {
    case 'TOGGLE_FAVORITE':
      const favoriteFilmIndex = state.favoritesMedia.findIndex(item => item.id === action.value.id)
      if (favoriteFilmIndex !== -1) {
        // Le film est déjà dans les favoris, on le supprime de la liste
        nextState = {
          ...state,
          favoritesMedia: state.favoritesMedia.filter( (item, index) => index !== favoriteFilmIndex)
        }
      }
      else {
        // Le film n'est pas dans les films favoris, on l'ajoute à la liste
        nextState = {
          ...state,
          favoritesMedia: [...state.favoritesMedia, action.value]
        }
      }
      return nextState || state
  default:
    return state
  }
}

export default toggleFavorite
