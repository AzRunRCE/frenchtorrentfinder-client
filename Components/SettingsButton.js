// API/KODI.js
import { Image, StyleSheet,TouchableOpacity} from 'react-native'
const HOST = "http://192.168.0.29:65220"; //Kodi HOST
import React, { Component } from 'react';

export default class SettingsButton extends Component {
  render (){
    const { _displayFavoriteImage } = this.props
    return (
      <TouchableOpacity   onPress={() => _displayFavoriteImage() } >
        
          <Image
              source={require('../Images/ic_settings.png')}
              style={styles.icon}
            />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  icon :{
            width: 30,
            height: 30,
            margin: 5
  }
})

