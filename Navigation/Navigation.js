// Navigation/Navigations.js

import React from 'react'
import { StyleSheet, Image } from 'react-native'
import { createAppContainer } from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import {createBottomTabNavigator} from 'react-navigation-tabs'

import Search from '../Components/Search'
import MovieDetail from '../Components/MovieDetail'
import TorrentResult from '../Components/TorrentResult'

import Favorites from '../Components/Favorites'
import News from '../Components/News'
import TvDetail from '../Components/Tv/TvDetail'
import SeasonDetail from '../Components/Tv/SeasonDetail'
import Settings from '../Components/Settings'
const _headerTitleStyle  = { color: '#DDDDDD' }
const _headerStyle = {backgroundColor:'#141414'}

const SearchStackNavigator = createStackNavigator({
  Search: {

    screen: Search,
    navigationOptions: {
      title: 'TV shows',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  MovieDetail: {
    screen: MovieDetail,
    navigationOptions: {
      title: 'Movie detail',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  TvDetail: {
    screen: TvDetail,
    navigationOptions: {
      title: 'TV shows detail',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  SeasonDetail: {
    screen: SeasonDetail,
    navigationOptions: {
      title: 'Season detail',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  TorrentResult: {
    screen: TorrentResult,
    navigationOptions: {
      title: 'Available sources',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  Settings: {
    screen: Settings,
    navigationOptions: {
      title: 'Settings',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  }
})

const FavoritesStackNavigator = createStackNavigator({
  Favorites: {
    screen: Favorites,
    navigationOptions: {
      title: 'Favorites',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  MovieDetail: {
    screen: MovieDetail,
    navigationOptions: {
      title: 'Movie detail',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  TvDetail: {
    screen: TvDetail,
    navigationOptions: {
      title: 'TV show detail',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  SeasonDetail: {
    screen: SeasonDetail,
    navigationOptions: {
      title: 'Season detail',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  TorrentResult: {
    screen: TorrentResult,
    navigationOptions: {
      title: 'Available sources',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  Settings: {
    screen: Settings,
    navigationOptions: {
      title: 'Settings',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  }
})

const NewsStackNavigator = createStackNavigator({
  News: {
    screen: News,
    navigationOptions: {
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle,
      title: 'Latest movies',
    },
  },
  MovieDetail: {
    screen: MovieDetail,
    navigationOptions: {
      title: 'Movie detail',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  TvDetail: {
    screen: TvDetail,
    navigationOptions: {
      title: 'TV show',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  SeasonDetail: {
    screen: SeasonDetail,
    navigationOptions: {
      title: 'Season detail',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  TorrentResult: {
    screen: TorrentResult,
    navigationOptions: {
      title: 'Available sources',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  },
  Settings: {
    screen: Settings,
    navigationOptions: {
      title: 'Settings',
      headerStyle: _headerStyle,
      headerTitleStyle: _headerTitleStyle
    }
  }
})

const MoviesTabNavigator = createBottomTabNavigator(
  {
    Search: {
      screen: SearchStackNavigator,
      navigationOptions: {
        headerStyle: _headerStyle,
        headerTitleStyle: _headerTitleStyle,
        tabBarIcon: () => {
          return <Image
            source={require('../Images/television.png')}
            style={styles.icon}/>
        }
      }
    },
    Favorites: {
      screen: FavoritesStackNavigator,
      navigationOptions: {
        headerStyle: _headerStyle,
        headerTitleStyle: _headerTitleStyle,
        tabBarIcon: () => {
          return <Image
            source={require('../Images/ic_favorite.png')}
            style={styles.icon}/>
        }
      }
    },
    News: {
      screen: NewsStackNavigator,
      navigationOptions: {
        headerStyle: _headerStyle,
        headerTitleStyle: _headerTitleStyle,
        tabBarIcon: () => {
          return <Image
            source={require('../Images/ic_fiber_new.png')}
            style={styles.icon}/>
        }
      }
    },
  },
  {
    tabBarOptions: {
      activeBackgroundColor: '#DDDDDD',
      inactiveBackgroundColor: '#141414',
      showLabel: false,
      showIcon: true
    }
  }
)

const styles = StyleSheet.create({
  icon: {
    width: 30,
    height: 30
  }
})

export default createAppContainer(MoviesTabNavigator)
