import React from 'react'
import { StyleSheet, View, Text, ActivityIndicator, ScrollView, Image, TouchableOpacity, Share, Alert, Platform, Button } from 'react-native'
import { getTvDetailFromApi, getImageFromApi } from '../../API/TMDBApi'
import moment from 'moment'
import numeral from 'numeral'
import { connect } from 'react-redux'
import EnlargeShrink from '../../Animations/EnlargeShrink'
import SeasonList from './SeasonList'
import {TV_CATEGORIE} from '../../API/TorrentFinder'
class TvDetail extends React.Component {

  static navigationOptions = ({ navigation }) => {
      const { params } = navigation.state
      if (params.media != undefined && Platform.OS === 'ios') {
        return {
            headerRight: <TouchableOpacity
                            style={styles.share_touchable_headerrightbutton}
                            onPress={() => params.sharemedia()}>
                            <Image
                              style={styles.share_image}
                              source={require('../../Images/ic_share.png')} />
                          </TouchableOpacity>
        }
      }
  }

  constructor(props) {
    super(props)
    this.state = {
      media: undefined,
      isLoading: false
    }

    this._toggleFavorite = this._toggleFavorite.bind(this)
  }

  _updateNavigationParams() {
    this.props.navigation.setParams({
      sharemedia: this._sharemedia,
      media: this.state.media
    })
  }

  componentDidMount() {

    this.setState({ isLoading: true })
    getTvDetailFromApi(this.props.navigation.state.params.idMedia).then(data => {
      this.setState({
        media: data,
        isLoading: false
      }, () => { this._updateNavigationParams() })
    })
  }

  _displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }

  _toggleFavorite() {
    const action = { type: "TOGGLE_FAVORITE", value: this.state.media }
    this.props.dispatch(action)
  }

  _displayFavoriteImage() {
    var sourceImage = require('../../Images/ic_favorite_border.png')
    var shouldEnlarge = false // Par défaut, si le media n'est pas en favoris, on veut qu'au clic sur le bouton, celui-ci s'agrandisse => shouldEnlarge à true
    if (this.props.favoritesMedia.findIndex(item => item.id === this.state.media.id) !== -1) {
      sourceImage = require('../../Images/ic_favorite.png')
      shouldEnlarge = true // Si le media est dans les favoris, on veut qu'au clic sur le bouton, celui-ci se rétrécisse => shouldEnlarge à false
    }
    return (
      <EnlargeShrink
        shouldEnlarge={shouldEnlarge}>
        <Image
          style={styles.favorite_image}
          source={sourceImage}
        />
      </EnlargeShrink>
    )
  }

  _displaymedia() {
    const { media } = this.state
    if (media != undefined) {
      return (
        <ScrollView style={styles.scrollview_container}>
          <Image
            style={styles.image}
            source={{uri: getImageFromApi(media.backdrop_path)}}
          />
          <Text style={styles.title_text}>{media.original_name}</Text>
          <TouchableOpacity
              style={styles.favorite_container}
              onPress={() => this._toggleFavorite()}>
              {this._displayFavoriteImage()}
          </TouchableOpacity>
          <Text style={styles.description_text}>{media.overview}</Text>
          <Text style={styles.default_text}>Sorti le {moment(new Date(media.release_date)).format('DD/MM/YYYY')}</Text>
          <Text style={styles.default_text}>Note : {media.vote_average} / 10</Text>
          <Text style={styles.default_text}>Nombre de votes : {media.vote_count}</Text>
          <Text style={styles.default_text}>Budget : {numeral(media.budget).format('0,0[.]00 $')}</Text>
          <Text style={styles.default_text}>Genre(s) : {media.genres.map(function(genre){
              return genre.name;
            }).join(" / ")}
          </Text>
          <Text style={styles.default_text}>Companie(s) : {media.production_companies.map(function(company){
              return company.name;
            }).join(" / ")}
          </Text>
          <SeasonList
          media={this.state.media}
          navigation={this.props.navigation}>
          </SeasonList>
        </ScrollView>
      )
    }
  }





  render() {
    return (
      <View style={styles.main_container}>
        {this._displayLoading()}
        {this._displaymedia()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#141414'
  },
  scrollview_container: {
    flex: 1,
    backgroundColor: '#141414'
  },
  image: {
    height: 169,
    margin: 5
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 35,
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 10,
    color: 'white',
    textAlign: 'center'
  },
  favorite_container: {
    alignItems: 'center',
  },
  description_text: {
    fontStyle: 'italic',
    color: 'white',
    margin: 5,
    marginBottom: 15
  },
  default_text: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
    color: 'white',
  },
  favorite_image:{
    flex: 1,
    width: null,
    height: null
  },
  share_touchable_floatingactionbutton: {
    position: 'absolute',
    width: 60,
    height: 60,
    right: 30,
    bottom: 30,
    borderRadius: 30,
    backgroundColor: '#e91e63',
    justifyContent: 'center',
    alignItems: 'center'
  },
  share_touchable_headerrightbutton: {
    marginRight: 8
  },
  share_image: {
    width: 30,
    height: 30
  }
})

const mapStateToProps = (state) => {
  return {
    favoritesMedia: state.toggleFavorite.favoritesMedia
  }
}

export default connect(mapStateToProps)(TvDetail)
