// Dependencies import
import React from 'react'
import { ScrollView, View } from 'react-native'
import SettingsStyle from '../Styles/SettingsStyle'

// Library main view definition
const SettingsPage = props => (
    <ScrollView style={ SettingsStyle.container }>
        <View style={ SettingsStyle.content }>
            {props.children}
        </View>     
    </ScrollView>
)

// Library exports
export * from  '../Row/SectionRow'
export * from '../Row/NavigateRow'
export * from '../Row/SwitchRow'
export * from '../Row/CheckRow'
export * from '../Row/SliderRow'
export * from '../Row/TextInputRow'

export default SettingsPage