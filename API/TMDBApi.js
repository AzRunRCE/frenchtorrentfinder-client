// API/TMDBApi.js

const API_TOKEN = "16b8297af5fa36d237245bb9a7e182f0";

export function getMoviesFromApiWithSearchedText (text, page) {
  const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text + "&page=" + page
  return fetch(url)
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function getMovieDetailFromApi (id) {
  return fetch('https://api.themoviedb.org/3/movie/' + id + '?api_key=' + API_TOKEN + '&language=fr')
    .then((response) => response.json())
    .catch((error) => console.error(error));
}

export function getBestMoviesFromApi (page) {
  return fetch('https://api.themoviedb.org/3/discover/movie?api_key=' + API_TOKEN + '&vote_count.gte=100&sort_by=release_date.desc&language=fr&page=' + page)
    .then((response) => response.json())
    .catch((error) => console.error(error));
}

export function getTvDetailFromApi (id) {
  return fetch('https://api.themoviedb.org/3/tv/' + id + '?api_key=' + API_TOKEN + '&language=fr')
    .then((response) => response.json())
    .catch((error) => console.error(error));
}

export function getTvsFromApiWithSearchedText (text, page) {
  const url = 'https://api.themoviedb.org/3/search/tv?api_key=' + API_TOKEN + '&language=fr&query=' + text + "&page=" + page
  return fetch(url)
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function getTvSeasonFromApi (id,season_number) {
  return fetch('https://api.themoviedb.org/3/tv/'+ id + '/season/' + season_number + '?api_key=' + API_TOKEN + '&language=fr')
    .then((response) => response.json())
    .catch((error) => console.error(error));
}

export function getBestTvFromApi (page) {
  return fetch('https://api.themoviedb.org/3/discover/tv?api_key=' + API_TOKEN + '&vote_count.gte=100&sort_by=vote_average.desc&language=fr&page=' + page)
    .then((response) => response.json())
    .catch((error) => console.error(error));
}
export function getImageFromApi (name) {
  return 'https://image.tmdb.org/t/p/w300' + name
}
