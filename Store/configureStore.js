// Store/configureStore.js

import { createStore , combineReducers} from 'redux'
import toggleFavorite from './Reducers/favoriteReducer'
import setAvatar from './Reducers/avatarReducer'
import savePreferences from './Reducers/settingsReducer'
//import { persistCombineReducers } from 'redux-persist'



export default createStore(combineReducers({toggleFavorite, setAvatar,savePreferences}))
