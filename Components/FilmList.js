// Components/FilmList.js

import React from 'react'
import { StyleSheet, FlatList, View,Dimensions } from 'react-native'
import FilmItem from './FilmItem'
import { connect } from 'react-redux'

class FilmList extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      medias: [],
      orientation: '',
      refresh: false
    }
  }


  _displayDetailForMedia = (section, idMedia) => {
    this.props.navigation.navigate(section + 'Detail', {idMedia: idMedia})
  }

  getOrientation(){
    if( Dimensions.get('window').width < Dimensions.get('window').height )
      {
        this.setState({ orientation: 'portrait' });
      }
      else
      {
        this.setState({ orientation: 'landscape' });
      }
  }

  componentDidMount(){
    this.getOrientation();

    Dimensions.addEventListener( 'change', () =>
    {
      this.getOrientation();
      this.setState({ refresh: !this.state.refresh });
    });

  }
  render() {
      return ( <View style={styles.MovieContainerGrid}>
        <FlatList
          style={styles.list}
          numColumns={3}
          data={this.props.medias}
          keyExtractor={(item) => item.id.toString()}
          extraData={this.state.refresh}
          renderItem={({item}) => (
            <FilmItem
              media={item}
              onPress={this._displayDetailForMedia}
              orientation = {this.state.orientation}
              section ={item.title === undefined ? 'Tv' : 'Movie' }
            />
          )}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            if (!this.props.favoriteList && this.props.page < this.props.totalPages) {
              this.props.loadMedias()
            }
          }}
        />
      </View>)
  }
}

const styles = StyleSheet.create({
  list: {
    flexWrap: 'wrap',
    backgroundColor: "#141414"
  },
  MovieContainerGrid:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#141414"
  }
})

const mapStateToProps = state => {
  return {
    favoritesFilm: state.toggleFavorite.favoritesFilm
  }
}

export default connect(mapStateToProps)(FilmList)
