// Components/News.js

import React from 'react'
import { StyleSheet, View, Image, TextInput,ActivityIndicator} from 'react-native'
import FilmList from './FilmList'
import { getBestTvFromApi,getTvsFromApiWithSearchedText } from '../API/TMDBApi'

import BaseScreen from './BaseScreen'
class Tv extends BaseScreen {
  constructor(props) {
    super(props)
    this.searchedText = ""
    this.page = 0
    this.totalPages = 0
    this.state = {
      medias: [],
      isLoading: false,
    }
    this._loadMedias = this._loadMedias.bind(this)
  }

  componentDidMount() {
    this._loadMedias()
  }

  _loadMedias(init) {
  if (this.searchedText.length > 0) {
    this.setState({ isLoading: true })
    getTvsFromApiWithSearchedText(this.searchedText, this.page+1).then(data => {
        this.page = data.page
        this.totalPages = data.total_pages
        this.setState({
          medias: [ ...this.state.medias, ...data.results ],
          isLoading: false
        })
    }).catch((error) =>{
      console.log(error);
    })
    return;
  }
  getBestTvFromApi(this.page+1).then(data => {
        this.page = data.page
        this.totalPages = data.total_pages
        this.setState({
          medias: [ ...this.state.medias, ...data.results ],
          isLoading: false
        })
    })
}
  _searchTextInputChanged(text) {
    this.searchedText = text
  }

  componentDidMount() {
    this._loadMedias(true)
  }

  _searchMedias() {
    this.page = 0
    this.totalPages = 0
    this.setState({
      medias: [],
    }, () => {
        this._loadMedias()
    })
  }

  

  _displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }
  render() {
    return (
      <View style={styles.main_container}>
        <View style={styles.SectionStyle}>
        <Image
            source={require('../Images/ic_search_ghostwhite.png')}
            style={styles.ImageStyle}/>
        <TextInput
          style={styles.textinput}
          placeholderTextColor="ghostwhite"
          placeholder='TV show name'
          onChangeText={(text) => { 
            this._searchTextInputChanged(text)
            this._searchMedias()
          } }
          onSubmitEditing={() => this._searchMedias()}
        />
       </View>
        <FilmList
          medias={this.state.medias}
          navigation={this.props.navigation}
          loadMedias={this._loadMedias}
          page={this.page}
          totalPages={this.totalPages}
          favoriteList={false} // Ici j'ai simplement ajouté un booléen à false pour indiquer qu'on n'est pas dans le cas de l'affichage de la liste des films favoris. Et ainsi pouvoir déclencher le chargement de plus de films lorsque l'utilisateur scrolle.
        />
        {this._displayLoading()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    backgroundColor: '#141414',
    
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#141414',
    padding: 0,
    margin:0
  },
  ImageStyle: {
    padding: 1,
    height: 30,
    width: 30,
  },

  textinput: {
    marginLeft: 5,
    marginRight: 5,
    height: 50,
    color: 'ghostwhite',
    borderColor: '#141414',
    borderWidth: 1,
    paddingLeft: 5,
    backgroundColor: '#141414'
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#141414'
  }
})


export default Tv