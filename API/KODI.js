// API/KODI.js



export function streamTorrent (HOST, uri) {
  let formdata = new FormData();
  formdata.append("uri", uri);
  formdata.append("file", "null");
  return fetch(HOST + '/playuri',
    {
      method: "POST",
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: formdata
    })
  .then((response) => {
    if (response.status == 200)
      return true;
    else
      return false;
  })
  .catch((error) => console.error(error));
}
