// Components/FilmList.js

import React from 'react'
import { StyleSheet, FlatList, View,Dimensions } from 'react-native'
import SeasonItem from './SeasonItem'
import { connect } from 'react-redux'

class SeasonList extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      seasons: [],
      orientation: '',
      refresh: false
    }
  }


  _displayDetailForSeason = (season_number) => {
    console.log("Display season " +season_number)
    this.props.navigation.navigate('SeasonDetail',
    {
      media: this.props.media,
      season_number: season_number
    }
    )
  }

  getOrientation(){
    if( Dimensions.get('window').width < Dimensions.get('window').height )
      {
        this.setState({ orientation: 'portrait' });
      }
      else
      {
        this.setState({ orientation: 'landscape' });
      }
  }

  componentDidMount(){
    this.getOrientation();

    Dimensions.addEventListener( 'change', () =>
    {
      this.getOrientation();
      this.setState({ refresh: !this.state.refresh });
    });

  }
  render() {
      return (
        <FlatList
          style={styles.list}
          data={this.props.media.seasons}
          keyExtractor={(item) => item.id.toString()}
          extraData={this.state.refresh}
          renderItem={({item}) => (
            <SeasonItem
              season={item}
              media={this.props.media}
              onPress={this._displayDetailForSeason}
              orientation = {this.state.orientation}
            />
          )}
        />)
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: "#141414"
  }
})



export default connect()(SeasonList)
